package hr.unizg.fer.diplpro.timeseries.forecasting.core.model

import hr.unizg.fer.diplpro.timeseries.forecasting.core.Statistics
import hr.unizg.fer.diplpro.timeseries.forecasting.core.feature.UDFS
import hr.unizg.fer.diplpro.timeseries.forecasting.data.DataManipulator
import hr.unizg.fer.diplpro.timeseries.forecasting.utils.Utils
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.sql.{DataFrame, SparkSession}

object BaselineRandomForestModel {
  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("MLModel").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    // reading data
    val data: DataFrame = DataManipulator.readCsvFile(spark, filePath = DataManipulator.electricityDataPath)

    // data preparation
    val (startDate, endDate) = Utils.getBounds(data, "time")
    var featureColumns: Array[String] = Array("priceLag")

    println("Min date: " + startDate.toString + ", max date: " + endDate.toString)

    val dataDate = data.select("time", "price").withColumnRenamed("time", "timeLag")
                      .withColumnRenamed("price", "priceLag")

    val newData = data.join(
      dataDate,
      UDFS.offsetYears(-1)(data.col("time")) === dataDate.col("timeLag")
    )

    newData.show(10)

    val (minTimestamp, maxTimestamp) = Utils.getBounds(data, "time")
    val splitTimestampTo = "2014-04-20 00:00:00"
    val splitTimestampFrom = "2014-04-20 01:00:00"
    var trainData: DataFrame = DataManipulator.dataFrameRange(newData, "time", minTimestamp, splitTimestampTo)
    var testData: DataFrame = DataManipulator.dataFrameRange(newData, "time", splitTimestampFrom, maxTimestamp)

    val assemblerQueue = new VectorAssembler()
      .setInputCols(featureColumns)
      .setOutputCol("Features")
    trainData = assemblerQueue.transform(trainData)
    testData = assemblerQueue.transform(testData)

    val regressor = new RandomForestRegressor()
      .setLabelCol("price")
      .setFeaturesCol("Features")

    val model = regressor.fit(trainData)
    val predictedDf = model.transform(testData)

    Statistics.evaluate(predictedDf, "price", "prediction", trainData)
  }
}
