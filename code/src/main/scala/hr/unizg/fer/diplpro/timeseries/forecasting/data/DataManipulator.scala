package hr.unizg.fer.diplpro.timeseries.forecasting.data

import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.{DataFrame, SparkSession, functions => F}

object DataManipulator {

  val electricityDataPath = "../data/electricity_data.csv"

  /**
    * Funkcija koja cita podatke iz zadane CSV datoteke i vraća DataFrame na kojem odmah pokuša nametnuti shemu podataka
    * @param filePath put do CSV datoteke sa podacima
    * @param separator separator podataka korišten u CSV datoteci
    * @param isHeaderPresent indikator postojanja zaglavlja u datoteci
    * @return DataFrame pročitanih podataka iz CSV datoteke
    */
  def readCsvFile(spark: SparkSession, filePath: String, separator: String = ";", isHeaderPresent: Boolean = true): DataFrame = {
    spark.read
      .option("header", isHeaderPresent)
      .option("separator", separator)
      .option("inferSchema", "true")
      .csv(filePath)
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("DataLoader")
      .master("local[2]")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")

    val data = readCsvFile(spark, electricityDataPath, ";", isHeaderPresent = true)
    data.show(10)
  }

  def dataFrameRange(df: DataFrame, columnName: String, from: Any, to: Any): DataFrame = {
    df.filter(F.col(columnName) >= F.lit(from))
      .filter(F.col(columnName) <= F.lit(to))
  }

  def dataLagForSeries(df: DataFrame, orderByColumn: String, featureColumnName: String, laggedColumnName: String, offset: Int) : DataFrame = {
    require(offset > 0)

    val window: WindowSpec = Window.orderBy(orderByColumn)
    val dfLagged: DataFrame = df.withColumn(laggedColumnName, F.lag(featureColumnName, offset, 0).over(window))

    dfLagged
  }

//  def sepDateToColumns(df: DataFrame, dateColumnName: String): DataFrame = {
//    val dataFrame = df.withColumn("year", )
//  }
}
