package hr.unizg.fer.diplpro.timeseries.forecasting.core

import com.cloudera.sparkts.EasyPlot
import hr.unizg.fer.diplpro.timeseries.forecasting.data.DataManipulator
import hr.unizg.fer.diplpro.timeseries.forecasting.utils.Utils
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SparkSession}

object Statistics {
  def evaluate(df: DataFrame, column: String, predicted: String, trainDf: DataFrame = null): Unit = {
    val eval = new RegressionEvaluator()
      .setLabelCol(column)
      .setPredictionCol(predicted)
    val mae = eval.setMetricName("mae").evaluate(df)
    val rmse = eval.setMetricName("rmse").evaluate(df)
    val r2 = eval.setMetricName("r2").evaluate(df)
    println("MAE: " + mae)
    println("RMSE: " + rmse)
    println("R2: " + r2)

    if (trainDf != null) {
      val trueValues = df.select(column).collect().map(row => row(0).toString.toDouble)
      val predictedValues = df.select(predicted).collect().map(row => row(0).toString.toDouble)
      val beforeVals = trainDf.select(column).collect().map(row => row(0).toString.toDouble).drop(trueValues.length)

      val predictedRange = beforeVals ++ predictedValues

      val fullRange = beforeVals ++ trueValues

      EasyPlot.ezplot(Seq(Vectors.dense(fullRange), Vectors.dense(predictedRange)), '-')
    } else {
      var trueValues = df.select(column).collect().map(row => row(0).toString.toDouble)
      var predictedValues = df.select(predicted).collect().map(row => row(0).toString.toDouble)

      EasyPlot.ezplot(Seq(Vectors.dense(trueValues), Vectors.dense(predictedValues)), '-')
    }
  }
}
