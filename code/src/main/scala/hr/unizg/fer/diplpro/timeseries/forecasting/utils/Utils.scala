package hr.unizg.fer.diplpro.timeseries.forecasting.utils

import java.sql.Timestamp

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions.{max, min}

object Utils {
  def getBounds(df: DataFrame, columnName: String): (Any, Any) =  {
    df.agg(min(columnName), max(columnName)).head match {
      case Row(a, b) => (a, b)
    }
  }
}
