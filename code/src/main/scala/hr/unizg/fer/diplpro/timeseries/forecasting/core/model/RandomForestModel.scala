package hr.unizg.fer.diplpro.timeseries.forecasting.core.model

import hr.unizg.fer.diplpro.timeseries.forecasting.core.Statistics
import hr.unizg.fer.diplpro.timeseries.forecasting.core.feature.UDFS
import hr.unizg.fer.diplpro.timeseries.forecasting.data.DataManipulator
import hr.unizg.fer.diplpro.timeseries.forecasting.utils.Utils
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.sql.{DataFrame, SparkSession, functions => F}

object RandomForestModel {
  val LABEL_COL = "price"

  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder.appName("MLModel").master("local[*]").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    val maxLag = 5

    var data: DataFrame = DataManipulator.readCsvFile(spark, DataManipulator.electricityDataPath)

    var featureColumns: Array[String] = Array("WeekOfYear", "DayOfWeek", "TimeOfTheDay", "summer")

    data = data.withColumn("WeekOfYear", F.weekofyear(F.col("time")))
    data = data.withColumn("DayOfWeek", F.dayofweek(F.col("time")))
    data = data.withColumn("TimeOfTheDay", F.hour(F.col("time")))
    val dataDate = data.select("time").withColumnRenamed("time", "time-avg")

    var laggedData = data.join(
      dataDate,
      UDFS.offsetDays(-1)(data.col("time")) === dataDate.col("time-avg")
    )

    for (col <- Array("price", "load", "volume", "wind", "solar")) {
      for (lag <- 1 to maxLag) {
        val realLag = lag
        val laggedFeatureName = String.format("%s-%s", col, realLag.toString)
        laggedData = DataManipulator.dataLagForSeries(laggedData, "time", col, laggedFeatureName, realLag)
        featureColumns :+= laggedFeatureName
      }
    }
    laggedData = laggedData.drop("volume", "load", "wind","solar","summer", "time", "price", "WeekOfYear", "DayOfWeek", "TimeOfTheDay")

    data = data.join(
      laggedData,
      UDFS.offsetYears(-1)(data.col("time")) === laggedData.col("time-avg")
    )

    val (minTimestamp, maxTimestamp) = Utils.getBounds(data, "time")
    val splitTimestampTo = "2014-04-20 00:00:00"
    val splitTimestampFrom = "2014-04-20 01:00:00"
    var trainData: DataFrame = DataManipulator.dataFrameRange(data, "time", minTimestamp, splitTimestampTo)
    var testData: DataFrame = DataManipulator.dataFrameRange(data, "time", splitTimestampFrom, maxTimestamp)

//      testData.show(10)

    val assemblerQueue = new VectorAssembler()
      .setInputCols(featureColumns)
      .setOutputCol("Features")
    trainData = assemblerQueue.transform(trainData)
    testData = assemblerQueue.transform(testData)

    // RF model
    val regressor = new RandomForestRegressor()
      .setLabelCol(LABEL_COL)
      .setFeaturesCol("Features")

    val model = regressor.fit(trainData)
    val predictedDf = model.transform(testData)

    Statistics.evaluate(predictedDf, LABEL_COL, "prediction", trainData)
  }
}
