package hr.unizg.fer.diplpro.timeseries.forecasting.core.feature

import java.sql.Timestamp
import java.util.Calendar

import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.{functions => F}

object UDFS {
  def offsetYears(offset: Int): UserDefinedFunction = {
    F.udf((date: Timestamp) => new Timestamp(date.getYear + offset, date.getMonth, date.getDate, date.getHours, date.getMinutes, date.getSeconds, date.getNanos))
  }

  def offsetDays(offset: Int): UserDefinedFunction = {
    val cal = Calendar.getInstance
    F.udf((date: Timestamp) => {
      cal.setTime(date)
      cal.add(Calendar.DAY_OF_WEEK, offset)

      new Timestamp(cal.getTime.getTime)
    })
  }

  def offsetMonths(offset: Int): UserDefinedFunction = {
    val cal = Calendar.getInstance
    F.udf((date: Timestamp) => {
      cal.setTime(date)
      cal.add(Calendar.MONTH, offset)

      new Timestamp(cal.getTime.getTime)
    })
  }
}
